export default {
  durationRules: [
    v => !!v || "Duration is required",
    v => /^((\d*(\.\d+)?)|(((?!00)\d*h)?((?!00)\d*m)?))$/.test(v) || 'Duration must be valid. Try 00h00m format.',
  ],
  projectRules: [
    v => !!v || "Project is required",
  ],
  remarksRules: [
    v => !!v || "Remarks is required",
  ],
};