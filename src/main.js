import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

Vue.filter('duration', function (value, unitLabel = '') {
  if (!value) return '';
  if (!Number.isInteger(value)) value = value.toFixed(2);
  if (!unitLabel) {
    unitLabel = value <= 1 ? ' hour' : ' hours';
  }
  return `${value}${unitLabel}`;
});

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
