import Vue from 'vue';

export const sortIncreasing = (a, b) => {
  if (a > b) {
      return 1;
  }
  if (b > a) {
      return -1;
  }
  return 0;
};

export const sortDecreasing = (a, b) => {
  if (a > b) {
      return -1;
  }
  if (b > a) {
      return 1;
  }
  return 0;
};

export const formatDuration = (durationString) => {
  // Get correct duration value from string
  const { groups: { hrsFloat, hrs, mins }} = /^((?<hrsFloat>\d*(\.\d+)?)|((?<hrs>\d*h)?(?<mins>\d*m)?))$/.exec(durationString);
  let duration = 0;
  if (hrsFloat) duration += parseFloat(hrsFloat);
  if (hrs) duration += parseInt(hrs);
  if (mins) duration += parseInt(mins)/60;
  return duration;
}

export const convertDuration = (duration) => {
  // Convert duration value to string
  const hours = parseInt(duration);
  const minutes = (duration - hours) * 60;
  let durationString = '';
  if (hours) durationString += `${hours}h`;
  if (minutes) durationString += `${minutes}m`;
  return durationString;
};

export const cloneObject = (obj) => {
  if (null == obj || "object" != typeof obj) return obj;
  return Vue.util.extend({}, obj);
}